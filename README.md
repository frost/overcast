# Overcast: a small selfhostable streaming service

Well, actually just the web UI and chat system. You'll need https://github.com/AirenSoft/OvenMediaEngine for the RTMP backend server.

### Dependencies

- [OvenMediaEngine][ome]
- [OvenPlayer](https://github.com/AirenSoft/OvenPlayer): bundled as a submodule
- npm `ws` package

### Setup

- Install [OvenMediaEngine][ome] somewhere. (This is the most annoying part.)
	- It's got documentation [over here](https://airensoft.gitbook.io/ovenmediaengine/getting-started). We built it from source, personally.
- Install the player component with `git submodule init` and `git submodule update`.
- Install WebSockets for the server side with `npm install` in the server directory.
- Set up the reverse proxy of your choice if you want TLS.
- Point your streaming program (OBS or whatever) at the OvenMediaEngine RTMP input, e.g. `rtmp://localhost/stream`. Set the stream key to `overcast`, or edit it to whatever you'd like in [client/setup-ovenplayer.js](client/setup-ovenplayer.js).
- Open ports:
	- 3333 for OvenMediaEngine's WebRTC signaling port.
	- 3478 for OvenMediaEngine's TURN server stuff.
	- your web port or 443, of course.

### Running

- Start OvenMediaEngine.
- Run the server: `node server.js <port>` in the server directory.
- Edit the 'video-title' file in the client directory to change the displayed stream name. Yes, this _really_ needs a better system.

### Obligatory chat security notes

The chat is encrypted in transit if you're using TLS. It is not end-to-end encrypted or anything; the server admin can read it all. Since you're presumably running this on a server you trust, it should be reasonably secure, but be aware that _all messages are logged_ in the server directory, `server/chatlogs/` (JSONL format if you want to take a look).

### License

It's public domain! Minus the dependencies, of course. See [COPYING.md](COPYING.md) for the details.

[ome]: https://github.com/AirenSoft/OvenMediaEngine
