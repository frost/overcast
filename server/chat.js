let ws = require("ws")
let fs = require("fs")

let reservedNicks = [
	"server",
	// not actually used but IRC does
	"NickServ",
	"ChanServ",
	// just covering all the troublemaker bases...
	"admin",
	"system"
]

let socket = null
let path = null
let history = []
historyFileHandle = null

function setup(httpServer) {
	setupHistory()
	logHistory({
		type: "server-start"
	})
	
	socket = new ws.WebSocketServer({server: httpServer})
	
	socket.on("connection", (client, request) => {
		if (/silent$|silent\/$/.test(request.url)) {
			console.log("Silent client connected")
			client.silent = true // this isn't actually a user, but something like the stream chat overlay
		} else {
			console.log("Chat client connected")
		}
		
		client.on("message", function receive(data) {
			let message = JSON.parse(data)
			message.sender = client.name
			message.plural = client.plural
			// console.log("Chat: Got message:", message)
			messageReceived(message, client)
		})
		client.on("close", () => {
			console.log(client.name, "disconnected")
			if (!client.silent) {
				broadcast({
					type: "leave",
					name: client.name,
					plural: client.isplural
				})
			}
		})
		
		client.send(JSON.stringify({
			type: "backlog",
			messages: history
		}))
		
		if (!client.silent) {
			assignClientName(client)
			
			// broadcast join event
			broadcast({
				type: "join",
				name: client.name,
				plural: client.isplural
			})
			
			client.send(JSON.stringify({
				isServer: true,
				type: "info",
				content: "Welcome! Use /nick to set your name."
			}, null, "\t"))
			client.send(JSON.stringify({
				isServer: true,
				type: "info",
				content: "You can use /plural for plural pronouns [/noplural to undo]."
			}, null, "\t"))
		}
		
		// broadcast online count
		broadcast({
			type: "stats",
			stats: {
				onlineCount: Array.from(socket.clients).filter((c) => !c.silent).length
			}
		})
	})
}

function broadcast(message) {
	message.timestamp = new Date()
	history.push(message)
	logHistory(message)
	socket.clients.forEach((client) => {
		// console.log("Broadcasting received message to client")
		client.send(JSON.stringify(message))
	})
}

function messageReceived(message, client) {
	message = handleChatCommands(message, client)
	if (message) {
		if (message.broadcast) {
			message.broadcast = undefined // internal, don't send it out
			if ((message.type === "message" || message.type === "action") && !message.sender) {
				// don't let people send messages without a name set
				console.log("Client has no name, returning error")
				message = {
					type: "error",
					content: "You need to set a name first."
				}
				client.send(JSON.stringify(message))
			} else {
				broadcast(message)
			}
		} else {
			message.broadcast = undefined // internal, don't send it out
			// console.log("Returning message to original client")
			client.send(JSON.stringify(message))
		}
	}
}

function handleChatCommands(message, client) {
	if (message.content == undefined) {
		console.log("Client error: no message content")
		message = {
			broadcast: false,
			type: "error",
			content: "Client error: Need to provide message content"
		}
		return message
	}
	
	if (!message.content.startsWith("/")) {
		// not a chat command
		message.broadcast = true
		return message
	}
	if (message.content.startsWith("//")) {
		// let you send messages beginning with a / by using //
		message.broadcast = true
		message.content = message.content.substring(1)
		return message
	}
	
	// it's a chat command
	
	if (message.content.startsWith("/nick")) {
		let name = message.content.replace(/^\/nick /, "")
		if (name === message.content || name === "") {
			message = {
				broadcast: false,
				type: "info",
				content: `You are currently known as ${client.name}.`
			}
			return message
		}
		
		if (reservedNicks.some((nick) => nick.toLowerCase() === name.toLowerCase())) {
			message = {
				broadcast: false,
				type: "error",
				content: `The name '${name}' is reserved.`
			}
			return message
		}
		
		// make sure the name isn't already taken
		if (Array.from(socket.clients).some((client) => client.name === name)) {
			message = {
				broadcast: false,
				type: "error",
				content: `${name} is already taken.`
			}
			return message
		}
		
		let oldName = client.name
		console.log(oldName, "is switching to name:", name)
		client.name = name
		message = {
			broadcast: true,
			type: "namechange",
			oldname: oldName,
			newname: name
		}
		return message
	}
	
	if (/^\/me[^a-zA-Z]/.test(message.content)) {
		let action = message.content.replace(/^\/me/, "")
		message.broadcast = true
		message.type = "action"
		message.content = action
		return message
	}
	
	if (message.content.startsWith("/plural")) {
		client.isplural = true
		message = {
			broadcast: true,
			type: "miscdata",
			sender: client.name,
			content: " set themselves plural."
		}
		return message
	}
	if (message.content.startsWith("/noplural")) {
		client.isplural = false
		message = {
			broadcast: true,
			type: "miscdata",
			sender: client.name,
			content: "set themself not plural."
		}
		return message
	}
	
	message = {
		broadcast: false,
		type: "error",
		content: "Unimplemented."
	}
	return message
}

function assignClientName(client) {
	let existingNames = Array.from(socket.clients).map((c) => c.name)
	let nameTemplate = "Anon"
	let n = 1
	let name = nameTemplate + n
	while (existingNames.some((c) => c === name)) {
		n++
		name = nameTemplate + n
	}
	// we must have a unique name now
	client.name = name
	return name
}

function setupHistory() {
	let now = new Date()
	// let formattedDate = `${now.getFullYear()}-${String(now.getMonth()+1).padStart(2, 0)}-${String(now.getDate()).padStart(2, 0)}_${String(now.getHours()).padStart(2, 0)}-${String(now.getMinutes()).padStart(2, 0)}`
	let formattedDate = `${now.getFullYear()}-${String(now.getMonth()+1).padStart(2, 0)}-${String(now.getDate()).padStart(2, 0)}`
	historyFileHandle = fs.createWriteStream(`chatlogs/${formattedDate}.jsonl`, {flags: "a"})
	
	/* log server shutdowns before terminating */
	process.on("SIGINT", () => {
		logHistory({
			type: "server-shutdown"
		})
		historyFileHandle.close()
		process.exit(0)
	})
	process.on("SIGTERM", () => {
		logHistory({
			type: "server-shutdown"
		})
		historyFileHandle.close()
		process.exit(0)
	})
}
function logHistory(message) {
	if (message.timestamp === undefined) {
		message.timestamp = new Date()
	}
	// console.log("Logging history event:", JSON.stringify(message))
	historyFileHandle.write(JSON.stringify(message) + "\n")
}

module.exports = {
	setup
}
