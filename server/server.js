let http = require("http")
let serveStatic = require("./serve-static.js")
let chat = require("./chat.js")
let path = require("path")
let fs = require("fs").promises

let port = process.argv[2]
if (!port) {
	console.error("Please give a port as an argument!")
	process.exit(1)
}

async function handleRequest(request, response) {
	let url = new URL(request.url, `http://${request.headers.host}`)
	console.log(`${request.method} ${url}`)
	if (url.pathname === "/websockets/chat") {
		
	}
	try {
		let content = await serveStatic.getContent("../client", request)
		if (String(content).startsWith("<!DOCTYPE html>")) {
			// template it!
			content = await templateReplace(String(content))
		}
		response.end(content)
	} catch (err) {
		console.log(err)
		response.writeHead((err.code === "ENOENT") ? 404 : 500)
		response.end(`${err.message}\n`)
	}
}

async function templateReplace(content) {
	let templateRegex = /{{(.*)}}/g
	let match = undefined
	while ((match = templateRegex.exec(content)) != null) {
		// console.log(match)
		let filename = path.normalize(match[1])
		let templateContents = String(await fs.readFile(path.join("../client/", filename)))
		// trim it to not mess up indent
		templateContents = templateContents.trim()
		// console.log("Template contents:", templateContents)
		
		let index = match.index
		let length = match[0].length
		content = content.substring(0, index) + templateContents + content.substring(index + length)
	}
	return content
}

let server = http.createServer(handleRequest)
server.listen(port, () => {
	console.log("Listening on port", port)
})

chat.setup(server)
