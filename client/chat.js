let socket = null
let chatElement = document.getElementById("chat")
let chatbox = document.getElementById("chatbox")
let connected = false
let streamMode = false
let history = []

function setupChat() {
	console.log("Setting up chat.")
	
	let params = new URLSearchParams(window.location.search)
	if (/1|true|yes/.test(params.get("streamMode"))) {
		console.log("Stream mode.")
		streamMode = true
		document.body.classList.add("streamMode")
	}
	
	let insecure = (window.location.protocol === "http:")
	let wsProtocol = (insecure) ? "ws://" : "wss://"
	let backend = (streamMode) ? "silent" : "chat"
	socket = new WebSocket(wsProtocol + window.location.host + "/websockets/" + backend)
	socket.addEventListener("open", () => {
		console.log("Chat: Websockets connection established.")
		connected = true
		chatbox.addEventListener("keydown", (event) => {
			if (event.key === "Enter") {
				let message = event.target.value
				if (message == "" || /^ +$/.test(message)) {
					return // it's blank, don't send it
				}
				sendMessage(message)
				event.target.value = ""
			}
		})
	})
	socket.addEventListener("error", (error) => {
		console.error("Chat: Error setting up websockets:", error)
		receiveMessage({
			type: "error",
			content: "Can't connect to chat server"
		})
	})
	socket.addEventListener("close", () => {
		if (connected) {
			connected = false
			receiveMessage({
				type: "error",
				content: "Disconnected from chat server."
			})
		}
	})
	socket.addEventListener("message", (event) => {
		let messageData = JSON.parse(event.data)
		// console.log("Chat: Got message", messageData)
		receiveMessage(messageData)
	})
}

function receiveMessage(messageData) {
	switch (messageData.type) {
		case "backlog":
			for (backlogMessage of messageData.messages) {
				receiveMessage(backlogMessage)
			}
			break
		case "stats":
			if (typeof updateStats !== "undefined") {
				updateStats(messageData)
			}
			break
		default:
			receiveChatMessage(messageData)
			break
	}
}

function receiveChatMessage(message) {
	history.push(message)
	updateDownloadHistoryButton()
	
	let newTextElement = document.createElement("message")
	newTextElement.classList.add(message.type)
	
	switch (message.type) {
		case "namechange": {
			let messageSpan = document.createElement("span")
			messageSpan.textContent = `${message.oldname} changed their name to ${message.newname}.`
			newTextElement.appendChild(messageSpan)
			break
		} case "join": {
			let messageSpan = document.createElement("span")
			let has = (message.plural) ? "have" : "has"
			messageSpan.textContent = `${message.name} ${has} connected.`
			newTextElement.appendChild(messageSpan)
			break
		} case "leave": {
			let messageSpan = document.createElement("span")
			let has = (message.plural) ? "have" : "has"
			messageSpan.textContent = `${message.name} ${has} disconnected.`
			newTextElement.appendChild(messageSpan)
			break
		} default: {
			if (message.sender) {
				let nameSpan = document.createElement("span")
				nameSpan.classList.add("name")
				nameSpan.textContent = (message.type == "message") ? `<${message.sender}> ` : `${message.sender}`
				newTextElement.appendChild(nameSpan)
			}
			
			let messageSpan = document.createElement("span")
			messageSpan.classList.add("message")
			messageSpan.textContent = message.content
			newTextElement.appendChild(messageSpan)
			break
		}
	}
	
	let needsScroll = false
	if (chatElement.scrollHeight - chatElement.scrollTop - chatElement.clientHeight < 8) {
		// we're scrolled to the bottom, see https://stackoverflow.com/a/44893438
		needsScroll = true
	}
	chatElement.appendChild(newTextElement)
	if (needsScroll) {
		newTextElement.scrollIntoView()
	}
}
function sendMessage(message) {
	// console.log("sending message:", message)
	socket.send(JSON.stringify({
		type: "message",
		content: message
	}, null, "\t"))
}

async function updateDownloadHistoryButton() {
	let historyAsText = history.map((m) => JSON.stringify(m)).join("\n")
	let encoded = encodeURIComponent(historyAsText)
	let url = `data:application/jsonl,${encoded}`
	
	let downloadButton = document.getElementById("download-chat-history")
	downloadButton.href = url
}

setupChat()
