let videoSourceHost = window.location.host.replace(/:[0-9]*$/, "")
let streamKey = "overcast"
let insecure = (window.location.protocol === "http:")
let wsProtocol = (insecure) ? "ws://" : "wss://"
let wsPort = (insecure) ? 3333 : 443

let player = OvenPlayer.create("player", {
	sources: [
		{
			label: "WebRTC stream",
			type: "webrtc",
			file: `${wsProtocol}${videoSourceHost}:${wsPort}/stream/${streamKey}`
		}
	]
})

function setTitle(title) {
	for (titleElement of document.querySelectorAll(".op-live-badge-lowlatency")) {
		titleElement.textContent = title
	}
}
async function updateTitle() {
	let title = await (await fetch("/video-title")).text()
	setTitle(title)
}

player.on("stateChanged", (info) => {
	console.log("player state changed:", info.newstate)
	updateTitle()
})
