let onlineCounter = document.getElementById("online-counter")

function updateStats(messageData) {
	let stats = messageData.stats
	console.log("Updating statistics:", stats)
	if (stats.onlineCount !== undefined) {
		onlineCounter.textContent = stats.onlineCount
	}
}
